const dino = document.getElementById("dino");
const cactus = document.getElementById("cactus");

document.addEventListener("keydown", function (event) {
  jupm();
});

function jupm() {
  if (dino.classList != "jump") {
    dino.classList.add("jump");
  }
  setTimeout(function () {
    dino.classList.remove("jump");
  }, 300);
}

let isAlive = setInterval(function () {
  let dinoTop = parseInt(window.getComputedStyle(dino).getPropertyValue("top"));
  let cactusLeft = parseInt(
    window.getComputedStyle(cactus).getPropertyValue("left")
  );
  if (cactusLeft < 50 && cactusLeft > 0 && dinoTop >= 130) {
    // alert("GAME OVER");
  }
}, 10);

// Мы приготовили проект, содержащий несколько файлов, в числе которых файл helper.js,
// экспортирующий вспомогательную функцию, которую вам и предстоит дописать так,
// чтобы она возвращала 10 первых символов строки, переданной в параметре функции.

const input = document.querySelector("#input");
const output = document.querySelector("#output");

input.addEventListener("input", (event) => {
  output.textContent = getFirstTen(event.currentTarget.value);

  ex2.textContent = dateConcat(20, "march");

  ex3.textContent = getExcerpt(event.currentTarget.value);

  ex4.textContent = sayHello(event.currentTarget.value);

  ex5.textContent = getFullName("Иван", "Иванов");

  ex6.innerHTML = spaceOdysseyTagline();

  ex7.innerHTML = renderTableRow("label", "value");

  ex8.textContent = getCapitalized("СOZEK");
  ex9.textContent = getCapitalized("сApTeK");
  ex10.textContent = getCapitalized("namer");
});

function getFirstTen(text) {
  return text.substring(0, 10); // введите что-нибудь в браузере и проверьте консоль
}
// Напишите функцию dateConcat, чтобы она возвращала строку, состоящую из числа и названия месяца.
function dateConcat(day, month) {
  return day + " " + month;
}
// Напишите функцию getExcerpt, чтобы она возвращала первые 10 символов из переданной ей строки с многоточием в конце.
function getExcerpt(text) {
  return text.substring(0, 10) + "...";
}
// Напишите функцию sayHello, чтобы она использовала переменную name в строке "Привет X" с помощью интерполяции.
function sayHello(name) {
  return `Привет, ${name}`;
}
// Напишите функцию getFullName, чтобы она возвращала имя и фамилию с помощью интерполяции.

function getFullName(first, last) {
  return `${first} ${last}`;
}

// Напишите функцию spaceOdysseyTagline, чтобы она возвращала строку:
// An epic drama
// of adventure
// and exploration
function spaceOdysseyTagline() {
  return `An epic drama</br>
of adventure </br>
and exploration`;
}

// Напишите функцию renderTableRow, чтобы она возвращала такой HTML:
// <tr>
//   <td>здесь лейбл</td>
//   <td>здесь значение</td>
// </tr>

// Вместо строчек «здесь лейбл» и «здесь значение» должны быть значения, принятые из параметров label и value.
function renderTableRow(label, value) {
  return `<tr>
  <td>${label}</td> 
  <td>${value}</td> 
</tr>`;
}

// Напишите функцию getCapitalized, чтобы она возвращала принятую в параметре word строку написанную
// с заглавной буквы. То есть превращать "СOZEK" в "Сozek", "сApTeK" в "Сaptek", а "namer" в "Namer".
function getCapitalized(word) {
  return word[0].toUpperCase() + word.substring(1).toLowerCase();
}
// Напишите функцию intoString, чтобы она конвертила переданное ей число в строку.
ex11.textContent = intoString(376006); // "376006"

function intoString(number) {
  return number.toString();
}

// Вам нужно написать код внутри вспомогательной функции getNextAge ,
//  чтобы она возвращала возраст пользователя в следующем году.

const age = document.querySelector("#age");
const nextAge = document.querySelector("#next");

age.addEventListener("input", () => {
  nextAge.textContent = getNextAge(age.value);
});
function getNextAge(age) {
  return Number.parseInt(age, 10) + 1;
}

// и допишите код вспомогательной функции getElementWidth,
//  чтобы она возвращала ширину элемента (как число) из полученной в параметре строки.

const element1 = document.querySelector("#element1");
const element2 = document.querySelector("#element2");

const element1Value = getComputedStyle(element1).width;
const element2Value = getComputedStyle(element2).width;

ex12.textContent = ("Ширина первого элемента", getElementWidth(element1Value));
ex13.textContent = ("Ширина второго элемента", getElementWidth(element2Value));

function getElementWidth(value) {
  return Number.parseInt(value, 10) + "px";
}
// Напишите функцию getRemainder, чтобы она возвращала остаток деления принятых в параметрах числа и делителя.
ex14.textContent = getRemainder(10, 3);
function getRemainder(number, divider) {
  return number % divider;
}
 
// Задайте переменную count с исходным значением 0.
// На следующей строчке увеличьте его на 1.
let count = 0;
ex14.textContent = count;
count++;
ex15.textContent = count;


// Определите переменную stableCoin со значением "$1".
// Сделайте так, чтобы ей нельзя было переназначить другое значение.

const stableCoin = "$1";